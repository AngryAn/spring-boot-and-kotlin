# Basic Kotlin WEB Application
Приложение создано для ознакомления со структурой SpringBoot Kotlin приложения

# Создано по инструкции
[Building web applications with Spring Boot and Kotlin](https://spring.io/guides/tutorials/spring-boot-kotlin/)

### Getting Started
Импортировать проект необходимочерез build.gradle.kts файл

### Автор
* Проказа Андрей [GitLab](https://gitlab.com/AngryAn) [Tlgrm](https://t.me/angryan)

